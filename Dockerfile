FROM alpine:latest
RUN mkdir /app
RUN apk add --no-cache python3 py3-pip docker-cli docker-cli-buildx curl bash jq git \
    && pip3 install --upgrade pip --break-system-packages \
    && pip3 install awscli --break-system-packages
